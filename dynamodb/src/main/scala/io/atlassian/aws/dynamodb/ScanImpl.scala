package io.atlassian.aws.dynamodb

import com.amazonaws.services.dynamodbv2.model.ScanRequest
import DynamoDB.{ ReadConsistency, SelectAttributes }
import collection.JavaConverters._

private[dynamodb] object ScanImpl {

  def forTable(tableName: String)(
    select: Option[SelectAttributes] = None,
    limit: Option[Int] = None,
    consistency: ReadConsistency = ReadConsistency.Eventual,
    exclusiveStartKey: Option[DynamoMap] = None): ScanImpl =
    ScanImpl(
      tableName,
      indexName = None,
      select,
      limit,
      consistency,
      exclusiveStartKey
    )
}

private[dynamodb] case class ScanImpl(
    tableName: String,
    indexName: Option[String],
    select: Option[SelectAttributes],
    limit: Option[Int],
    consistency: ReadConsistency,
    exclusiveStartKey: Option[DynamoMap]) {
  def asScanRequest: ScanRequest = {
    val req =
      new ScanRequest()
        .withTableName(tableName)
        .withConsistentRead(ReadConsistency.asBool(consistency))

    indexName.foreach { idx => req.withIndexName(idx) }
    limit.foreach { req.setLimit(_) }
    select.foreach { s => req.withSelect(SelectAttributes.asSelect(s)) }
    exclusiveStartKey.foreach { esk => req.setExclusiveStartKey(esk.asJava) }

    req
  }
}
