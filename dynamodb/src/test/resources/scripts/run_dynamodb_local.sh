#!/bin/sh -e

while getopts ':dp:t:' option
do
    case $option in
        p   )   portOpt=$OPTARG;;
        t   )   timeoutOpt=$OPTARG;;
    esac
done

SCRIPT_DIR=`dirname "$0"`
PORT=${portOpt:-8000}
TIMEOUT_SECONDS=${timeoutOpt:-30}

# find the timeout command ('timeout' on ubuntu, 'gtimeout' on MacOS X)
for cmd in gtimeout timeout; do
  if type ${cmd} > /dev/null 2>&1; then
    TIMEOUT_CMD=${cmd}
    break
  fi
done
if [ -z "${TIMEOUT_CMD}" ]; then
  echo "It seems you don't have the timeout binary. If you're on MacOS X, try 'brew install coreutils'.";
  exit 1
fi

echo "Starting Local DynamoDB..."

eval "docker run -d --rm --name dynamodb_${PORT} -p ${PORT}:8000 -m 768M amazon/dynamodb-local"

SUCCESS=$?

AWS_ENVS="-e AWS_ACCESS_KEY_ID=access -e AWS_SECRET_ACCESS_KEY=secret -e AWS_DEFAULT_REGION=us-west-2"
if [ ! -z "$BITBUCKET_DOCKER_HOST_INTERNAL" ]; then
  echo "running in pipelines"
  AWS="docker run --rm --add-host host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL $AWS_ENVS amazon/aws-cli"
else
  echo "running locally"
  AWS="docker run --rm $AWS_ENVS amazon/aws-cli"
fi
${TIMEOUT_CMD} --foreground ${TIMEOUT_SECONDS} bash -c "until ${AWS} dynamodb list-tables --endpoint-url http://host.docker.internal:${PORT} | grep -E \"TableNames\" > /dev/null; do echo -n .; sleep 2; done; exit 0"

# For some reason we need this echo for Scala Process to return correctly when spinning up a DynamoDB within DynamoDB Specs"
echo "Started Local DynamoDB..."
exit $SUCCESS
